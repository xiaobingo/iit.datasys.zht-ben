################
echo ""
echo "######### clean zht_ben process"
parallel-ssh -t 0 -A --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'source /users/xiaobing/.bashrc; killall -9 zht_ben;'

#fuser -k 50000/tcp


echo ""
echo "######### rm /tmp/*.start,  /tmp/*.ben, /tmp/*.val"
parallel-ssh -t 0 -A --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'rm /tmp/*.start; rm /tmp/*.ben; rm /tmp/*.val'

bash rmlogmisc.sh


echo ""
echo "######### stop MPI all"
bash stopMPIAll.sh

echo ""
echo "######### stop ZHT all"
bash stopZHTAll.sh
