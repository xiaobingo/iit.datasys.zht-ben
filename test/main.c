#include <stdio.h>
#include <stdlib.h>
#include "sample.pb-c.h"


ZZPack* init_zzpack(uint key, int has_key, int has_val, int num_comments) {

	//ZZPack *zzpack = (ZZPack*) calloc(1, sizeof(ZZPack));
	ZZPack *zzpack = (ZZPack*) calloc(1, sizeof(ZZPack));

	zzpack__init(zzpack);

	if (has_key)
		zzpack->key = key;

	if (has_val) {

		zzpack->value = (char*) calloc(100, sizeof(char));
	}

	zzpack->n_comments = num_comments;

	if (zzpack->n_comments > 0) {

		zzpack->comments = (char**) calloc(num_comments, sizeof(char*));

		int i;
		for (i = 0; i < zzpack->n_comments; i++) {

			zzpack->comments[i] = (char*) calloc(100, sizeof(char));
		}
	}

	/*	unsigned len = package__get_packed_size(&zzpack);
	 char *buf = (char*) calloc(len, sizeof(char));
	 zzpack__pack(&zzpack, buf);*/

	return zzpack;

}

int main(int argc, char **argv) {

	ZZPack *pzzpack = init_zzpack(1, 1, 1, 0);

	unsigned len = zzpack__get_packed_size(pzzpack);

	printf("len: %u\n", len);

}
