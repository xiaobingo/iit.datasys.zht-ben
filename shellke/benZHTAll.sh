###################

echo ""
echo "######### zht ben ready?"
bash benReady.sh

echo ""
echo "######### rm /tmp/#.host.ben"
parallel-ssh -t 0 -A --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'rm /tmp/*.ben'

echo ""
echo "######### zht benchmark"
#parallel-ssh -t 0 -A --par=1000 --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'source /users/xiaobing/.bashrc; /users/xiaobing/zht/src/shell/zht_ben -z /users/xiaobing/zht/src/shell/zht.conf -n /users/xiaobing/zht/src/shell/neighbor.conf -o 1000 2>&1 > /tmp/zht.ben'

i=1
for line in $(cat neighbor.mpi.conf)
do 
echo "$i.$line"
source benZHT.sh $i $line &
let "i=i+1"
done 

