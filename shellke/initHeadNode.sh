echo "creating /users/xiaobing/zht/log/"
mkdir /users/xiaobing/zht/log/

echo ""
echo "creating links to executables or conf files"
ln -s ../zht-mpiserver
ln -s ../zht-mpibroker
ln -s ../zhtserver
ln -s ../zht_ben
ln -s ../zht.conf
ln -s ../neighbor.conf

echo ""
echo "installing parallel-ssh"
sudo dpkg -i /users/xiaobing/Downloads/pssh_2.2.2-0ubuntu1_all.deb

echo ""
echo "supress the ssh authenticity test"
sudo cp ssh_config /etc/ssh/ssh_config
#sudo echo "UserKnownHostsFile /dev/null" >> /etc/ssh/ssh_config
#sudo echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

echo ""
echo "init bashrc"
bash initBashrc.sh
