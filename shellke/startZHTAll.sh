################

echo ""
echo "######### rm /tmp/zht.start"
parallel-ssh -t 0 -A --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'rm /tmp/zht.start'


echo ""
echo "######### start zhtserver"
#parallel-ssh -t 0 -A --par=1000 --hosts=neighbor.mpi.conf -O StrictHostKeyChecking=no -O UserKnownHostsFile=/dev/null -o /users/xiaobing/zht/boot/pshout -e /users/xiaobing/zht/boot/psherr 'source /users/xiaobing/.bashrc; /users/xiaobing/zht/src/shell/zhtserver -z /users/xiaobing/zht/src/shell/zht.conf -n /users/xiaobing/zht/src/shell/neighbor.conf 2>&1 >> /tmp/zht.start &'



for line in $(cat neighbor.mpi.conf)
do 
echo "$line" 
source startZHT.sh $line &
sleep 0.01
done 

echo "start ZHT all done"
